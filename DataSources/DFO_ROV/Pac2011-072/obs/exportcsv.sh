dbs=$(ls *.mdb)
for db in $dbs; do
    out="${db%.mdb}.csv"
    mdb-export $db data > $out
done
