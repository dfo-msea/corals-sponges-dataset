SELECT
H.Source AS SURVEY,
H.Key AS HKey,
H.TowNum AS SET_ID,
H.Year AS YEAR,
H.Month AS MONTH,
H.Day AS DAY,
H.StatArea AS MAJOR_STAT_AREA_CODE,
H.LatDegStart + H.LatMinStart / 60 AS START_LATITUDE,
-(H.LongDegStart + H.LongMinStart / 60) AS START_LONGITUDE,
H.LatDegEnd + H.LatMinEnd / 60 AS END_LATITUDE,
-(H.LongDegEnd + H.LongMinEnd / 60) AS END_LONGITUDE,
(IIF(IsNull(H.MinDepth),H.MaxDepth,H.MinDepth) + IIF(IsNull(H.MaxDepth),H.MinDepth,H.MaxDepth))/2  AS DEPTH,
H.Depth_Unit,
L.Description AS GEAR,
C.Species AS SPECIES_CODE
FROM
(Headers H INNER JOIN ByCatch C ON H.Key = C.HKey)
INNER JOIN luSource L ON H.Source = L.SourceCode
WHERE H.LatDegStart IS NOT NULL AND H.LongMinEnd IS NOT NULL;
