SELECT
FF.FISHERY_NAME AS FISHERY_SECTOR,
T.TRIP_ID,
E.FISHING_EVENT_ID AS EVENT_ID,
YEAR(E.START_DATE) AS YEAR,
MONTH(E.START_DATE) AS MONTH,
E.START_DATE AS DATE,
E.START_LATITUDE,
E.START_LONGITUDE,
E.END_LATITUDE,
E.END_LONGITUDE,
(IsNull(E.END_DEPTH_FM, E.START_DEPTH_FM) + IsNull(E.START_DEPTH_FM, E.END_DEPTH_FM))/2  AS DEPTH,
TT.TRIP_TYPE_NAME,
C.SPECIES_CODE,
S.SPECIES_SCIENTIFIC_NAME AS SPECIES_SCIENCE_NAME,
S.SPECIES_COMMON_NAME
FROM GFFOS.dbo.GF_TRIP T
    INNER JOIN
    GFFOS.dbo.GF_FISHING_EVENT E ON T.TRIP_ID = E.TRIP_ID
    INNER JOIN
    GFFOS.dbo.GF_FE_CATCH C ON E.FISHING_EVENT_ID = C.FISHING_EVENT_ID
	INNER JOIN
    GFFOS.dbo.GF_TRIP_FISHERY F ON T.TRIP_ID = F.TRIP_ID
	INNER JOIN
    GFFOS.dbo.TRIP_TYPE TT ON F.TRIP_TYPE_CODE = TT.TRIP_TYPE_CODE
	INNER JOIN
    GFFOS.dbo.FISHERY FF ON F.FISHERY_CODE = FF.FISHERY_CODE
INNER JOIN
    GFFOS.dbo.SPECIES S ON S.SPECIES_CODE = C.SPECIES_CODE
WHERE START_LATITUDE IS NOT NULL AND START_LONGITUDE IS NOT NULL
-- insert where here
--AND S.SPECIES_CODE = '2A0'
ORDER BY YEAR, SPECIES_SCIENCE_NAME
