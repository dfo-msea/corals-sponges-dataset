# Presence Absence Observations of Coral and Sponge Groups

__Main author:__  Jessica Nephin  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: jessica.nephin@dfo-mpo.gc.ca | tel: 250.363.6564


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
  + [Coral and Sponge Groups](#coral-and-sponge-groups)
  + [Combining Data Sources](#combining-data-sources)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
To produce a dataset of coral and sponge groups presence and absence observation on the BC shelf to be used to develop species distribution models for Sensitive Benthic Area identification.


## Summary
Corals and sponges are sampled both directly (e.g. ROV observations) and indirectly (e.g. trawl by-catch) by a variety of DFO programs and partners. Thus there was a need to compile observations from a variety of sources within DFO Pacific (Groundfish data unit, Shellfish data unit, MSEA section) and externally (commercial fisheries logs, museum records, NOAA).

Those data sources contained observations of corals and sponges captured from a variety of sampling methods and gear types (dive, ROV, traps, longline and bottom trawls) that are likely to vary widely in their detectability of corals and sponges. While all gear types considered can sometimes detect presence, gear types such as trap and longline are likely to have very low detectability of coral and sponges, therefore would not accurately represent their absence. Absence observations were sourced from a selection of ROV, dive and synoptic bottom trawl research surveys that were explicitly designed to sample corals and sponges or all epibenthic species.

For a summary of the coral and sponge dataset by group see 'AllCombined/Summary_CoralSponge_Dataset.md' within this project.


## Status
Completed


## Contents
This project contains code within the AllCombined sub-directory that combines coral and sponge observations from multiple sources and code within DataSources/ sub-directories that queries groundfish and shellfish databases, geo-references ROV annotations, standardises data from different surveys, runs QA/QC checks and removes errors, assign coral and sponge observations into groups and creates unique fields to identify surveys and sampling events.


## Methods
### Coral and Sponge Groups
Coral and sponge observations were available at various levels of taxonomic identification from phyla to species. Species were identified by DFO Hart Codes. When a Hart Code did not exist one was created (e.g. 0A1, 0A2, ... 0B9). A list of Hart Codes and species descriptions can be found in AllCoralSponge_HartCodes_Groups_Master.csv. To combine these observations they were categorized into hierarchical groups (3 levels, 11 total). These groups were based off of previous work by Chu et al. 2020. Level 1 groups were corals and sponges. Level 2 groups for sponges were demosponges, glass sponges and calcareous sponges and level 2 groups for corals were soft corals, stony corals, hydrocorals, sea pens and black corals. Soft corals were broken into a 3rd level group: gorgonian corals.
*   Coral  
    *  Soft coral  
        *  Gorgonian  
    *  Stony coral  
    *  Hydrocoral  
    *  Sea pen  
    *  Black coral  
*   Sponge  
    *  Demosponge  
    *  Glass sponge  
    *  Calcareous sponge  


### Combining Data Sources
Attribute fields were retained whenever possible to ensure that records in the combined coral and sponge dataset would be traceable back to the sampling event in their source dataset. There were some attribute fields which existed in some data sources but not others (e.g. sampling event or trip ID) but the majority of sources had the basic attribute fields: unique identifier, survey name, date, gear, depth. At a minimum to be included in the combined dataset observations needed a latitude and longitude position of the sampling event that had at least 3 decimal digits in decimal degrees units. Observations from years prior to 1995 were filtered out to reduce imprecise latitude and longitude positions.

A unique identifier (PID) field that represents each sampling event was created for the combined dataset using the following attribute fields: Source, Survey, Longitude, Latitude, Latitude_End, Longitude_End. For each unique identifier (PID), attributes: Depth, Year, Month, Event_ID and Trip_ID were not always the same. This was caused by different definitions of events within the data sources and potentially some errors in the source data. To reconcile this the minimum value was taken for Depth, Year and Month for each unique PID and Event_ID and Trip_ID were concatenated with a dash.

Separate feature layers were required for each coral and sponge groups (11 in total) as each group datasets differed in the number of records. This variation was caused by the detectability difference between sampling gears and therefore not all surveys or sampling gear types were able to represent both presence and absences. For example, the sea pen feature layer may have a presence point in a location but the soft coral feature layer may not have a point in that same location unless it was also present there or the observation came from a data source that used a sampling gear that was considered accurately represent the absence of coral and sponges (see Uncertainty).


## Requirements
Esri ArcMap and R software were used. Access to DFO Groundfish and Shellfish data servers as well as input data from Shellfish_ROV and MSEA DFO networked drives are required. R packages dplyr, rgdal, arcgisbinding, stringr and DBI.


## Caveats
The dataset does not include any observations from DFO ROV seamount surveys.


## Uncertainty
Absence records may not accurately represent true absences of coral and sponge groups. Absence records were included based on the assumption that groundfish synoptic bottom trawl surveys and DFO dive and ROV surveys with study objectives related to corals and sponges had a high enough detectability of corals and sponges to accurately represent their absence.


## Acknowledgements
Jackson Chu, Maria Surry, Katie Gale, Leslie Barton


## References
Chu, Jackson W.F., J Nephin, S Georgian, A Knudby, C Rooper and K.S.P. Gale. (2019). Modelling the environmental niche space and distributions of cold-water corals and sponges in the Canadian northeast Pacific Ocean. Deep Sea Research Part I Oceanographic Research Papers. https://doi.org/10.1016/j.dsr.2019.06.009
