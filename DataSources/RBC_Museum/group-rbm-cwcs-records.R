require(dplyr)


#----------------------------------------------------------------------------#
# load species data
dat <- read.csv("RBCMI_SpongeCoral_2014.csv", 
                header=T, stringsAsFactors = F, row.names = 1)

# Remove dates before 1995
dat <- dat %>% filter(Year >= 1995)

# load link to cwcs groups
link <- read.csv("RBM_spongecorals_groupslink_wCodes.csv", 
                 header=T, stringsAsFactors = F)


# remove trailing spaces
link$Species_Name <- gsub("\\s+$", "", link$Species_Name)
dat$Species_Name <- gsub("\\s+$", "", dat$Species_Name)

# merge
gdat <- merge(dat, link[,1:2], by="Species_Name")


# merge with coral and sponge groups
grps <- read.csv("AllCoralSponge_HartCodes_Groups.csv", header=T, stringsAsFactors = F)
dfgrp <- merge(gdat[,c(1:3,12:21)], grps, by.x= "Species_Codes", by.y = "SPECIES_CODE", all.x=T)
dfgrp$Group[is.na(dfgrp$Group)] <- ""
gdat <- dfgrp


# Convert month to numeric
gdat$Month <- match(paste0(substring(gdat$Month, 1, 1), tolower(substring(gdat$Month, 2,3))), month.abb)

# Add attributes
gdat$Type <- "Research"
gdat$Source <- "RBCM"
gdat$Trip_ID <- substr(gdat$IDNo, 6, 8)
gdat$Event_ID <- substr(gdat$IDNo, 10, 14)
gdat$Gear <- ""
gdat$Survey <- ""
gdat$Depth <- NA
gdat$Latitude <- gdat$BestLat
gdat$Longitude <- gdat$BestLong
gdat$Species_Common_Name <- ""


# select columns
rbcmdat <- gdat %>% select(Type, Source, Survey,Trip_ID, Event_ID,
                           Year, Month, Gear, Depth, Latitude, Longitude, 
                           Phylum, Class, Subclass, Order, Family, Genus, Species,
                           Species_Code=Species_Codes, Species_Science_Name=SPECIES_SCI_NAME, 
                           Species_Common_Name, Parent_Group, Group, Subgroup)

# Save
save(rbcmdat, file="rbcm_dat.RData")
