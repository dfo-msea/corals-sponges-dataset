SELECT
H.Source AS SURVEY,
H.Key AS HKey,
H.SetNum AS SET_ID,
H.Year AS YEAR,
H.Month AS MONTH,
H.Day AS DAY,
H.StatArea AS MAJOR_STAT_AREA_CODE,
H.StartLatDeg + H.StartLatMin / 60 AS START_LATITUDE,
-(H.StartLongDeg + H.StartLongMin / 60) AS START_LONGITUDE,
H.EndLatDeg + H.EndLatMin / 60 AS END_LATITUDE,
-(H.EndLongDeg + H.EndLongMin / 60) AS END_LONGITUDE,
(IIF(IsNull(H.MinDepth),H.MaxDepth,H.MinDepth) + IIF(IsNull(H.MaxDepth),H.MinDepth,H.MaxDepth))/2  AS DEPTH,
H.DepthUnit,
G.Description AS GEAR,
C.species AS SPECIES_CODE
FROM
((Headers H INNER JOIN LF ON (LF.HKey = H.Key) AND (H.Key = LF.HKey))
INNER JOIN luGearCode G ON (G.Code = LF.GearCode) AND (LF.GearCode = G.Code))
INNER JOIN ByCatch C ON (LF.HKey = C.H_key) AND (LF.Line = C.Line)
WHERE H.StartLatDeg IS NOT NULL AND H.EndLongDeg IS NOT NULL;
