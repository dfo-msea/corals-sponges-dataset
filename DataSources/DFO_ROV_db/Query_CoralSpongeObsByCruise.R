library('DBI')
library('odbc')


# Get list of all coral and sponge surveys and subset cruise list
# then loop through and make csv files for each

# Probably need to query obs events and positions separately so that start and end times
# of events can be expanded before joining with xy. This is mostly important for
# habitat events that tend to span time intervals.


# to do
# - update query to pull obs event and habitat events and combine original label into a single column
#  or maybe just query obs events, habitat events, and positions, then expand time segments and then
# link to positions. Maybe attempt this with another survey that has start and end times?


# Connect to db
con <- DBI::dbConnect(odbc::odbc(),
                      server = '139.177.195.65',
                      driver = 'PostgreSQL Unicode',
                    #  database = 'msea',
                      UID = 'msea',#rstudioapi::askForPassword('Database user'),
                      PWD = 'Lophelia1', #rstudioapi::askForPassword('Database password'),
                      port = 8000
)


# Coral and sponge species to search for
csnames <- read.csv('D:/SpeciesData/Inverts_Algae/Corals_Sponges/DataSources/DFO_ROV_db/Coral_Sponge_SearchNames.csv')


# Set up regex string of coral and sponge names
cs_str <- paste(csnames$names, collapse = '|')


# Get all events with xy for a single cruise
# Need to include the habitat event table because sponge and coral observations can be 
# included as biocover or substrate
sql1 <- "with t as (
  select p.timestamp, e.id, e.start_time, e.end_time, d.name as dive_name, 
  s.original_label, s.scientific_name, s.common_name, s.hart_code, s.aphia_id, o.count, 
  b.name as abundance, p.position, r.name as trip_id, r.leg, ud.name as dominant_substrate,
  us.name as subdominant_substrate, x.name as biocover
  from 
  rov.event e
  left join rov.observation_event o on o.event_id=e.id
  left join rov.habitat_event h on h.event_id=e.id  
  left join rov.substrate ud on ud.id=h.dominant_substrate_id
  left join rov.substrate us on us.id=h.subdominant_substrate_id
  left join rov.biocover x on x.id=h.biocover_id
  left join rov.annotation_protocol_taxon a on a.id=o.annotation_protocol_taxon_id
  left join shared.taxon s on s.id=a.taxon_id
  left join rov.abundance b on b.id=o.abundance_id
  inner join rov.position p on p.timestamp = e.start_time
  inner join rov.dive d on d.id = e.dive_id
  inner join rov.cruise r on r.id=d.cruise_id
  inner join rov.instrument_config c on p.instrument_config_id=c.id
  inner join rov.instrument i on c.instrument_id=i.id
  -- limit to rov positions through instrument types
  where (i.id=3 or i.id=27) and r.name='Pac2008-057' -- and s.original_label ~* '!HERE!'
)
select id, trip_id, leg, dive_name, start_time, end_time, timestamp, 
original_label, scientific_name, common_name, hart_code, aphia_id, count, abundance, 
dominant_substrate, subdominant_substrate, biocover, position 
from t
order by start_time, id"
# Add coral and sponge search
sql2 <- sub('!HERE!', cs_str, sql1) 
csxy <- odbc::dbGetQuery(con, sql2)
str(csxy)
head(csxy)
summary(csxy)
#should be 413 but missing some positions


# Get all events for a single cruise
# Need to include the habitat event table because sponge and coral observations can be 
# included as biocover or substrate
sql1 <- "with t as (
  select e.id, e.start_time, e.end_time, d.name as dive_name, 
  so.original_label as observation_label, sh.original_label as habitat_label,
  so.scientific_name, so.common_name, so.hart_code, so.aphia_id, o.count, 
  b.name as abundance, r.name as trip_id, r.leg, ud.name as dominant_substrate,
  us.name as subdominant_substrate, x.name as biocover
  from 
  rov.event e
  left join rov.observation_event o on o.event_id=e.id
  left join rov.habitat_event h on h.event_id=e.id  
  left join rov.substrate ud on ud.id=h.dominant_substrate_id
  left join rov.substrate us on us.id=h.subdominant_substrate_id
  left join rov.biocover x on x.id=h.biocover_id
  left join rov.annotation_protocol_taxon ao on ao.id=o.annotation_protocol_taxon_id
  left join rov.annotation_protocol_taxon ah on ah.id=h.annotation_protocol_taxon_id
  left join shared.taxon so on so.id=ao.taxon_id
  left join shared.taxon sh on sh.id=ah.taxon_id
  left join rov.abundance b on b.id=o.abundance_id
  inner join rov.dive d on d.id = e.dive_id
  inner join rov.cruise r on r.id=d.cruise_id
  where r.name='Pac2008-057' -- and s.original_label ~* '!HERE!'
)
select id, trip_id, leg, dive_name, start_time, end_time, 
observation_label, habitat_label, scientific_name, common_name, hart_code, aphia_id, count, abundance, 
dominant_substrate, subdominant_substrate, biocover 
from t
order by start_time, id"
# Add coral and sponge search
sql2 <- sub('!HERE!', cs_str, sql1) 
cse <- odbc::dbGetQuery(con, sql2)
str(cse)
head(cse)
summary(cse)
#should be 413 

# Parse position
pts <- strsplit( gsub('[[]|[]]','', csxy$position), ',')
csxy$x <- as.numeric(sapply(pts, '[', 1)) # Get first element 
csxy$y <- as.numeric(sapply(pts, '[', 2)) # Get first element 

# Find species matches to csnames table
# Fuzzy match, uses the closest match
labels <- unique(csxy$original_label)
m <- data.frame(label=labels, lookup_name='')
for (l in labels){
  matches <- agrep(l,csnames$names,ignore.case=TRUE,value=TRUE,max.distance = 0.13)
  m$lookup_name[m$label==l] <-matches[1]
  cat(l, '=', paste(matches, collapse=', '), '\n')
}

# Merge species code to csxy with lookup
getspcodes <- csnames$codes
names(getspcodes) <- csnames$names
m$species_code <- getspcodes[m$lookup_name]
dat <- merge(csxy, m, by.x = 'original_label', by.y='label')
str(dat)
head(dat)
summary(dat)

# Disconnect
dbDisconnect(con)

