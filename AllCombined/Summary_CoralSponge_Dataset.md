# Summary of coral and sponge group datasets
## Coral 
OGR data source with driver: OpenFileGDB 
Source: "E:\SpeciesData\Inverts_Algae\Corals_Sponges\AllCombined\Data\AllSources_PA_CoralSpongeGroups.gdb", layer: "Coral"
with 88449 features
It has 14 fields

### Overall prevalence                               
 |          | Count | Percent |
 | -------- | ----- | ------- |
 |  Absence | 66203 |    74.8 |
 | Presence | 22246 |    25.2 |
 |   #Total | 88449 |   100.0 |
 |     <NA> |     0 |     0.0 |

### Data source                                                   
 |                    Source | Absence | Presence |
 | ------------------------- | ------- | -------- |
 |                   CrabBio |         |       19 |
 |                       FOS |         |     1836 |
 |                     GFBio |    5234 |     2090 |
 |                   GFVideo |         |      916 |
 |                 MSEA_Dive |    3374 |     1510 |
 |                  MSEA_ROV |   10774 |     2167 |
 | NOAA_CoralSponge_Database |         |     1751 |
 |                     PacHL |         |      201 |
 |                  PacSable |         |       69 |
 |                  PacTrawl |         |     2138 |
 |                      RBCM |         |      106 |
 |             Shellfish_ROV |   46821 |     9354 |
 |             TannerCrabBio |         |       81 |
 |       UVic_VENUS_CDuPreez |         |        8 |
 |                    #Total |   66203 |    22246 |

### Gear type                                      
 |         Gear | Absence | Presence |
 | ------------ | ------- | -------- |
 |              |         |     1975 |
 | Bottom trawl |    5234 |     1499 |
 |         Dive |    3374 |     1510 |
 |     Longline |         |      398 |
 |          ROV |   57595 |    14114 |
 | Shrimp trawl |         |      361 |
 |         Trap |         |      127 |
 |        Trawl |         |     2262 |
 |       #Total |   66203 |    22246 |

### Year                                    
 | Year range | Absence | Presence |
 | ---------- | ------- | -------- |
 |  1995-1999 |         |      635 |
 |  2000-2004 |     566 |     2350 |
 |  2005-2009 |   14457 |     4556 |
 |  2010-2014 |   19174 |     6609 |
 |  2015-2019 |   32006 |     8061 |
 |  2020-2024 |         |       35 |
 |     #Total |   66203 |    22246 |

### Month                                
 |  Month | Absence | Presence |
 | ------ | ------- | -------- |
 |      1 |         |      129 |
 |      2 |         |      285 |
 |      3 |   10160 |     2049 |
 |      4 |     187 |      745 |
 |      5 |    6708 |     2692 |
 |      6 |   14548 |     3814 |
 |      7 |   12020 |     2542 |
 |      8 |    1358 |     2736 |
 |      9 |   14334 |     4690 |
 |     10 |    6888 |     2031 |
 |     11 |         |      430 |
 |     12 |         |      103 |
 | #Total |   66203 |    22246 |


## Sponge 
OGR data source with driver: OpenFileGDB 
Source: "E:\SpeciesData\Inverts_Algae\Corals_Sponges\AllCombined\Data\AllSources_PA_CoralSpongeGroups.gdb", layer: "Sponge"
with 109118 features
It has 14 fields

### Overall prevalence                                
 |          |  Count | Percent |
 | -------- | ------ | ------- |
 |  Absence |  57825 |      53 |
 | Presence |  51293 |      47 |
 |   #Total | 109118 |     100 |
 |     <NA> |      0 |       0 |

### Data source                                                   
 |                    Source | Absence | Presence |
 | ------------------------- | ------- | -------- |
 |                   CrabBio |         |        9 |
 |                       FOS |         |     1589 |
 |                     GFBio |    4553 |     2686 |
 |                   GFVideo |         |     1525 |
 |                 MSEA_Dive |    4800 |       84 |
 |                  MSEA_ROV |   10792 |     2149 |
 | NOAA_CoralSponge_Database |         |      778 |
 |                     PacHL |         |       82 |
 |                  PacSable |         |       22 |
 |                  PacTrawl |         |     2969 |
 |                      RBCM |         |       69 |
 |           ScallopTrawlBio |         |       29 |
 |             Shellfish_ROV |   37680 |    39233 |
 |             TannerCrabBio |         |       54 |
 |       UVic_VENUS_CDuPreez |         |       15 |
 |                    #Total |   57825 |    51293 |

### Gear type                                      
 |         Gear | Absence | Presence |
 | ------------ | ------- | -------- |
 |              |         |     1658 |
 | Bottom trawl |    4553 |     2066 |
 |         Dive |    4800 |       84 |
 |     Longline |         |      537 |
 |          ROV |   48472 |    43678 |
 | Shrimp trawl |         |      128 |
 |         Trap |         |       69 |
 |        Trawl |         |     3073 |
 |       #Total |   57825 |    51293 |

### Year                                    
 | Year range | Absence | Presence |
 | ---------- | ------- | -------- |
 |  1995-1999 |         |      651 |
 |  2000-2004 |     483 |     2798 |
 |  2005-2009 |   14081 |     9824 |
 |  2010-2014 |   14456 |    26923 |
 |  2015-2019 |   28805 |    11087 |
 |  2020-2024 |         |       10 |
 |     #Total |   57825 |    51293 |

### Month                                
 |  Month | Absence | Presence |
 | ------ | ------- | -------- |
 |      1 |         |      335 |
 |      2 |         |     2557 |
 |      3 |   10480 |    11123 |
 |      4 |     281 |      554 |
 |      5 |    6326 |     2954 |
 |      6 |   11700 |    10345 |
 |      7 |   12483 |     1931 |
 |      8 |    1463 |     2619 |
 |      9 |    9458 |    10700 |
 |     10 |    5634 |     7087 |
 |     11 |         |      990 |
 |     12 |         |       98 |
 | #Total |   57825 |    51293 |


## Glass_sponges 
OGR data source with driver: OpenFileGDB 
Source: "E:\SpeciesData\Inverts_Algae\Corals_Sponges\AllCombined\Data\AllSources_PA_CoralSpongeGroups.gdb", layer: "Glass_sponges"
with 98026 features
It has 14 fields

### Overall prevalence                               
 |          | Count | Percent |
 | -------- | ----- | ------- |
 |  Absence | 72045 |    73.5 |
 | Presence | 25981 |    26.5 |
 |   #Total | 98026 |   100.0 |
 |     <NA> |     0 |     0.0 |

### Data source                                                   
 |                    Source | Absence | Presence |
 | ------------------------- | ------- | -------- |
 |                   CrabBio |         |        1 |
 |                       FOS |         |      307 |
 |                     GFBio |    5792 |      725 |
 |                   GFVideo |         |     1486 |
 |                 MSEA_Dive |    4879 |        5 |
 |                  MSEA_ROV |   11427 |     1514 |
 | NOAA_CoralSponge_Database |         |       29 |
 |                     PacHL |         |        3 |
 |                  PacTrawl |         |      459 |
 |                      RBCM |         |       38 |
 |           ScallopTrawlBio |         |       16 |
 |             Shellfish_ROV |   49947 |    21373 |
 |             TannerCrabBio |         |       10 |
 |       UVic_VENUS_CDuPreez |         |       15 |
 |                    #Total |   72045 |    25981 |

### Gear type                                      
 |         Gear | Absence | Presence |
 | ------------ | ------- | -------- |
 |              |         |      345 |
 | Bottom trawl |    5792 |      606 |
 |         Dive |    4879 |        5 |
 |     Longline |         |      114 |
 |          ROV |   61374 |    24400 |
 | Shrimp trawl |         |        1 |
 |         Trap |         |        8 |
 |        Trawl |         |      502 |
 |       #Total |   72045 |    25981 |

### Year                                    
 | Year range | Absence | Presence |
 | ---------- | ------- | -------- |
 |  1995-1999 |         |       56 |
 |  2000-2004 |     622 |      406 |
 |  2005-2009 |   16064 |     6698 |
 |  2010-2014 |   19938 |    14937 |
 |  2015-2019 |   35421 |     3884 |
 |     #Total |   72045 |    25981 |

### Month                                
 |  Month | Absence | Presence |
 | ------ | ------- | -------- |
 |      1 |         |      113 |
 |      2 |         |     1908 |
 |      3 |   11103 |     9752 |
 |      4 |     281 |       84 |
 |      5 |    8005 |      556 |
 |      6 |   15761 |     3507 |
 |      7 |   13128 |      625 |
 |      8 |    1672 |      481 |
 |      9 |   14586 |     3480 |
 |     10 |    7509 |     4751 |
 |     11 |         |      712 |
 |     12 |         |       12 |
 | #Total |   72045 |    25981 |


## Demosponges 
OGR data source with driver: OpenFileGDB 
Source: "E:\SpeciesData\Inverts_Algae\Corals_Sponges\AllCombined\Data\AllSources_PA_CoralSpongeGroups.gdb", layer: "Demosponges"
with 85829 features
It has 14 fields

### Overall prevalence                               
 |          | Count | Percent |
 | -------- | ----- | ------- |
 |  Absence | 63017 |    73.4 |
 | Presence | 22812 |    26.6 |
 |   #Total | 85829 |   100.0 |
 |     <NA> |     0 |     0.0 |

### Data source                                                   
 |                    Source | Absence | Presence |
 | ------------------------- | ------- | -------- |
 |                       FOS |         |      252 |
 |                     GFBio |    6082 |      279 |
 |                   GFVideo |         |       39 |
 |                 MSEA_Dive |    4810 |       74 |
 |                  MSEA_ROV |   12205 |      736 |
 | NOAA_CoralSponge_Database |         |       12 |
 |                     PacHL |         |        4 |
 |                  PacSable |         |        2 |
 |                  PacTrawl |         |      156 |
 |                      RBCM |         |       34 |
 |           ScallopTrawlBio |         |        3 |
 |             Shellfish_ROV |   39920 |    21208 |
 |       UVic_VENUS_CDuPreez |         |       13 |
 |                    #Total |   63017 |    22812 |

### Gear type                                      
 |         Gear | Absence | Presence |
 | ------------ | ------- | -------- |
 |              |         |      286 |
 | Bottom trawl |    6082 |      265 |
 |         Dive |    4810 |       74 |
 |     Longline |         |       18 |
 |          ROV |   52125 |    22004 |
 | Shrimp trawl |         |        2 |
 |        Trawl |         |      163 |
 |       #Total |   63017 |    22812 |

### Year                                    
 | Year range | Absence | Presence |
 | ---------- | ------- | -------- |
 |  1995-1999 |         |       24 |
 |  2000-2004 |     621 |       94 |
 |  2005-2009 |   14522 |     2301 |
 |  2010-2014 |   16162 |    12827 |
 |  2015-2019 |   31712 |     7564 |
 |  2020-2024 |         |        2 |
 |     #Total |   63017 |    22812 |

### Month                                
 |  Month | Absence | Presence |
 | ------ | ------- | -------- |
 |      1 |         |       11 |
 |      2 |         |      398 |
 |      3 |   10901 |     1176 |
 |      4 |     281 |       46 |
 |      5 |    6798 |     1690 |
 |      6 |   12459 |     7590 |
 |      7 |   13279 |      276 |
 |      8 |    1715 |     1277 |
 |      9 |   11338 |     7877 |
 |     10 |    6246 |     2400 |
 |     11 |         |       63 |
 |     12 |         |        8 |
 | #Total |   63017 |    22812 |


## Calcareous_sponge 
OGR data source with driver: OpenFileGDB 
Source: "E:\SpeciesData\Inverts_Algae\Corals_Sponges\AllCombined\Data\AllSources_PA_CoralSpongeGroups.gdb", layer: "Calcareous_sponge"
with 79199 features
It has 14 fields

### Overall prevalence                               
 |          | Count | Percent |
 | -------- | ----- | ------- |
 |  Absence | 78981 |    99.7 |
 | Presence |   218 |     0.3 |
 |   #Total | 79199 |   100.0 |
 |     <NA> |     0 |     0.0 |

### Data source                                       
 |        Source | Absence | Presence |
 | ------------- | ------- | -------- |
 |           FOS |         |       24 |
 |         GFBio |    6318 |       19 |
 |       GFVideo |         |       16 |
 |     MSEA_Dive |    4866 |       18 |
 |      MSEA_ROV |   12941 |          |
 |         PacHL |         |        1 |
 |      PacSable |         |        1 |
 |      PacTrawl |         |       46 |
 |          RBCM |         |        3 |
 | Shellfish_ROV |   54856 |       89 |
 | TannerCrabBio |         |        1 |
 |        #Total |   78981 |      218 |

### Gear type                                      
 |         Gear | Absence | Presence |
 | ------------ | ------- | -------- |
 |              |         |       27 |
 | Bottom trawl |    6318 |       19 |
 |         Dive |    4866 |       18 |
 |     Longline |         |        2 |
 |          ROV |   67797 |      105 |
 |        Trawl |         |       47 |
 |       #Total |   78981 |      218 |

### Year                                    
 | Year range | Absence | Presence |
 | ---------- | ------- | -------- |
 |  1995-1999 |         |        5 |
 |  2000-2004 |     634 |       45 |
 |  2005-2009 |   16578 |       20 |
 |  2010-2014 |   22697 |       15 |
 |  2015-2019 |   39072 |      133 |
 |     #Total |   78981 |      218 |

### Month                                
 |  Month | Absence | Presence |
 | ------ | ------- | -------- |
 |      1 |         |        3 |
 |      2 |         |        4 |
 |      3 |   11532 |        3 |
 |      4 |     281 |       13 |
 |      5 |    8413 |        4 |
 |      6 |   17311 |       95 |
 |      7 |   13467 |       48 |
 |      8 |    1755 |        7 |
 |      9 |   17754 |       23 |
 |     10 |    8468 |        3 |
 |     11 |         |       14 |
 |     12 |         |        1 |
 | #Total |   78981 |      218 |


## Stony_corals 
OGR data source with driver: OpenFileGDB 
Source: "E:\SpeciesData\Inverts_Algae\Corals_Sponges\AllCombined\Data\AllSources_PA_CoralSpongeGroups.gdb", layer: "Stony_corals"
with 80649 features
It has 14 fields

### Overall prevalence                               
 |          | Count | Percent |
 | -------- | ----- | ------- |
 |  Absence | 77509 |    96.1 |
 | Presence |  3140 |     3.9 |
 |   #Total | 80649 |   100.0 |
 |     <NA> |     0 |     0.0 |

### Data source                                                   
 |                    Source | Absence | Presence |
 | ------------------------- | ------- | -------- |
 |                       FOS |         |      342 |
 |                     GFBio |    6333 |       17 |
 |                   GFVideo |         |        5 |
 |                 MSEA_Dive |    3576 |     1308 |
 |                  MSEA_ROV |   12697 |      244 |
 | NOAA_CoralSponge_Database |         |      252 |
 |                     PacHL |         |       93 |
 |                  PacSable |         |       29 |
 |                  PacTrawl |         |      405 |
 |                      RBCM |         |        9 |
 |             Shellfish_ROV |   54903 |      427 |
 |             TannerCrabBio |         |        1 |
 |       UVic_VENUS_CDuPreez |         |        8 |
 |                    #Total |   77509 |     3140 |

### Gear type                                      
 |         Gear | Absence | Presence |
 | ------------ | ------- | -------- |
 |              |         |      353 |
 | Bottom trawl |    6333 |       12 |
 |         Dive |    3576 |     1308 |
 |     Longline |         |      100 |
 |          ROV |   67600 |      933 |
 |         Trap |         |       27 |
 |        Trawl |         |      407 |
 |       #Total |   77509 |     3140 |

### Year                                    
 | Year range | Absence | Presence |
 | ---------- | ------- | -------- |
 |  1995-1999 |         |      243 |
 |  2000-2004 |     634 |      297 |
 |  2005-2009 |   16539 |      333 |
 |  2010-2014 |   22179 |     1086 |
 |  2015-2019 |   38157 |     1158 |
 |  2020-2024 |         |       23 |
 |     #Total |   77509 |     3140 |

### Month                                
 |  Month | Absence | Presence |
 | ------ | ------- | -------- |
 |      1 |         |       27 |
 |      2 |         |       85 |
 |      3 |   11288 |      465 |
 |      4 |     206 |      126 |
 |      5 |    7919 |      634 |
 |      6 |   17352 |      172 |
 |      7 |   13437 |      149 |
 |      8 |    1560 |      540 |
 |      9 |   17279 |      790 |
 |     10 |    8468 |       71 |
 |     11 |         |       58 |
 |     12 |         |       23 |
 | #Total |   77509 |     3140 |


## Hydrocorals 
OGR data source with driver: OpenFileGDB 
Source: "E:\SpeciesData\Inverts_Algae\Corals_Sponges\AllCombined\Data\AllSources_PA_CoralSpongeGroups.gdb", layer: "Hydrocorals"
with 79557 features
It has 14 fields

### Overall prevalence                               
 |          | Count | Percent |
 | -------- | ----- | ------- |
 |  Absence | 75599 |      95 |
 | Presence |  3958 |       5 |
 |   #Total | 79557 |     100 |
 |     <NA> |     0 |       0 |

### Data source                                                   
 |                    Source | Absence | Presence |
 | ------------------------- | ------- | -------- |
 |                       FOS |         |       25 |
 |                     GFBio |    6332 |        8 |
 |                 MSEA_Dive |    4815 |       69 |
 |                  MSEA_ROV |   12878 |       63 |
 | NOAA_CoralSponge_Database |         |        4 |
 |             Shellfish_ROV |   51574 |     3789 |
 |                    #Total |   75599 |     3958 |

### Gear type                                      
 |         Gear | Absence | Presence |
 | ------------ | ------- | -------- |
 |              |         |       27 |
 | Bottom trawl |    6332 |        5 |
 |         Dive |    4815 |       69 |
 |          ROV |   64452 |     3853 |
 |         Trap |         |        3 |
 |        Trawl |         |        1 |
 |       #Total |   75599 |     3958 |

### Year                                    
 | Year range | Absence | Presence |
 | ---------- | ------- | -------- |
 |  2000-2004 |     635 |        2 |
 |  2005-2009 |   16020 |      565 |
 |  2010-2014 |   21508 |     1610 |
 |  2015-2019 |   37436 |     1781 |
 |     #Total |   75599 |     3958 |

### Month                                
 |  Month | Absence | Presence |
 | ------ | ------- | -------- |
 |      2 |         |       23 |
 |      3 |   11469 |      129 |
 |      4 |     279 |        2 |
 |      5 |    7839 |      580 |
 |      6 |   16123 |     1344 |
 |      7 |   13486 |       15 |
 |      8 |    1748 |       86 |
 |      9 |   16643 |     1291 |
 |     10 |    8012 |      487 |
 |     11 |         |        1 |
 | #Total |   75599 |     3958 |


## Soft_corals 
OGR data source with driver: OpenFileGDB 
Source: "E:\SpeciesData\Inverts_Algae\Corals_Sponges\AllCombined\Data\AllSources_PA_CoralSpongeGroups.gdb", layer: "Soft_corals"
with 81640 features
It has 14 fields

### Overall prevalence                               
 |          | Count | Percent |
 | -------- | ----- | ------- |
 |  Absence | 73524 |    90.1 |
 | Presence |  8116 |     9.9 |
 |   #Total | 81640 |   100.0 |
 |     <NA> |     0 |     0.0 |

### Data source                                                   
 |                    Source | Absence | Presence |
 | ------------------------- | ------- | -------- |
 |                   CrabBio |         |        3 |
 |                       FOS |         |      620 |
 |                     GFBio |    5966 |      497 |
 |                   GFVideo |         |       56 |
 |                 MSEA_Dive |    4810 |       74 |
 |                  MSEA_ROV |   11165 |     1776 |
 | NOAA_CoralSponge_Database |         |      499 |
 |                     PacHL |         |       96 |
 |                  PacSable |         |       33 |
 |                  PacTrawl |         |      836 |
 |                      RBCM |         |       57 |
 |             Shellfish_ROV |   51583 |     3532 |
 |             TannerCrabBio |         |       37 |
 |                    #Total |   73524 |     8116 |

### Gear type                                      
 |         Gear | Absence | Presence |
 | ------------ | ------- | -------- |
 |              |         |      692 |
 | Bottom trawl |    5966 |      401 |
 |         Dive |    4810 |       74 |
 |     Longline |         |      181 |
 |          ROV |   62748 |     5831 |
 |         Trap |         |       53 |
 |        Trawl |         |      884 |
 |       #Total |   73524 |     8116 |

### Year                                    
 | Year range | Absence | Presence |
 | ---------- | ------- | -------- |
 |  1995-1999 |         |      179 |
 |  2000-2004 |     615 |      775 |
 |  2005-2009 |   15384 |     2048 |
 |  2010-2014 |   21690 |     1566 |
 |  2015-2019 |   35835 |     3541 |
 |  2020-2024 |         |        7 |
 |     #Total |   73524 |     8116 |

### Month                                
 |  Month | Absence | Presence |
 | ------ | ------- | -------- |
 |      1 |         |       57 |
 |      2 |         |      118 |
 |      3 |   10527 |     1195 |
 |      4 |     278 |      116 |
 |      5 |    8272 |      397 |
 |      6 |   16826 |      862 |
 |      7 |   12328 |     1379 |
 |      8 |    1633 |      773 |
 |      9 |   15986 |     2160 |
 |     10 |    7674 |      933 |
 |     11 |         |      100 |
 |     12 |         |       26 |
 | #Total |   73524 |     8116 |


## Sea_pens 
OGR data source with driver: OpenFileGDB 
Source: "E:\SpeciesData\Inverts_Algae\Corals_Sponges\AllCombined\Data\AllSources_PA_CoralSpongeGroups.gdb", layer: "Sea_pens"
with 83966 features
It has 14 fields

### Overall prevalence                               
 |          | Count | Percent |
 | -------- | ----- | ------- |
 |  Absence | 76085 |    90.6 |
 | Presence |  7881 |     9.4 |
 |   #Total | 83966 |   100.0 |
 |     <NA> |     0 |     0.0 |

### Data source                                                   
 |                    Source | Absence | Presence |
 | ------------------------- | ------- | -------- |
 |                   CrabBio |         |       16 |
 |                       FOS |         |      853 |
 |                     GFBio |    5583 |     1582 |
 |                   GFVideo |         |      855 |
 |                 MSEA_Dive |    4643 |      241 |
 |                  MSEA_ROV |   12890 |       51 |
 | NOAA_CoralSponge_Database |         |      878 |
 |                     PacHL |         |       20 |
 |                  PacSable |         |       12 |
 |                  PacTrawl |         |     1007 |
 |                      RBCM |         |       50 |
 |             Shellfish_ROV |   52969 |     2295 |
 |             TannerCrabBio |         |       21 |
 |                    #Total |   76085 |     7881 |

### Gear type                                      
 |         Gear | Absence | Presence |
 | ------------ | ------- | -------- |
 |              |         |      903 |
 | Bottom trawl |    5583 |     1097 |
 |         Dive |    4643 |      241 |
 |     Longline |         |      126 |
 |          ROV |   65859 |     4047 |
 | Shrimp trawl |         |      357 |
 |         Trap |         |       51 |
 |        Trawl |         |     1059 |
 |       #Total |   76085 |     7881 |

### Year                                    
 | Year range | Absence | Presence |
 | ---------- | ------- | -------- |
 |  1995-1999 |         |      261 |
 |  2000-2004 |     586 |     1338 |
 |  2005-2009 |   16202 |     1571 |
 |  2010-2014 |   21640 |     2622 |
 |  2015-2019 |   37657 |     2084 |
 |  2020-2024 |         |        5 |
 |     #Total |   76085 |     7881 |

### Month                                
 |  Month | Absence | Presence |
 | ------ | ------- | -------- |
 |      1 |         |       48 |
 |      2 |         |       67 |
 |      3 |   11505 |      237 |
 |      4 |     258 |      513 |
 |      5 |    7815 |     1196 |
 |      6 |   16264 |     1624 |
 |      7 |   13244 |     1052 |
 |      8 |    1642 |     1310 |
 |      9 |   17323 |      904 |
 |     10 |    8034 |      604 |
 |     11 |         |      273 |
 |     12 |         |       53 |
 | #Total |   76085 |     7881 |


## Black_corals 
OGR data source with driver: OpenFileGDB 
Source: "E:\SpeciesData\Inverts_Algae\Corals_Sponges\AllCombined\Data\AllSources_PA_CoralSpongeGroups.gdb", layer: "Black_corals"
with 79360 features
It has 14 fields

### Overall prevalence                               
 |          | Count | Percent |
 | -------- | ----- | ------- |
 |  Absence | 78965 |    99.5 |
 | Presence |   395 |     0.5 |
 |   #Total | 79360 |   100.0 |
 |     <NA> |     0 |     0.0 |

### Data source                                                   
 |                    Source | Absence | Presence |
 | ------------------------- | ------- | -------- |
 |                       FOS |         |       22 |
 |                     GFBio |    6319 |       19 |
 |                 MSEA_Dive |    4884 |          |
 |                  MSEA_ROV |   12817 |      124 |
 | NOAA_CoralSponge_Database |         |      216 |
 |                      RBCM |         |       14 |
 |             Shellfish_ROV |   54945 |          |
 |                    #Total |   78965 |      395 |

### Gear type                                      
 |         Gear | Absence | Presence |
 | ------------ | ------- | -------- |
 |              |         |       53 |
 | Bottom trawl |    6319 |       19 |
 |         Dive |    4884 |          |
 |          ROV |   67762 |      322 |
 |        Trawl |         |        1 |
 |       #Total |   78965 |      395 |

### Year                                    
 | Year range | Absence | Presence |
 | ---------- | ------- | -------- |
 |  2000-2004 |     635 |       34 |
 |  2005-2009 |   16582 |      152 |
 |  2010-2014 |   22688 |       71 |
 |  2015-2019 |   39060 |      138 |
 |     #Total |   78965 |      395 |

### Month                                
 |  Month | Absence | Presence |
 | ------ | ------- | -------- |
 |      2 |         |        1 |
 |      3 |   11408 |      124 |
 |      4 |     281 |        3 |
 |      5 |    8413 |        3 |
 |      6 |   17392 |        1 |
 |      7 |   13499 |        8 |
 |      8 |    1742 |      214 |
 |      9 |   17762 |       36 |
 |     10 |    8468 |        2 |
 |     11 |         |        2 |
 |     12 |         |        1 |
 | #Total |   78965 |      395 |


## Gorgonian 
OGR data source with driver: OpenFileGDB 
Source: "E:\SpeciesData\Inverts_Algae\Corals_Sponges\AllCombined\Data\AllSources_PA_CoralSpongeGroups.gdb", layer: "Gorgonian"
with 79906 features
It has 14 fields

### Overall prevalence                               
 |          | Count | Percent |
 | -------- | ----- | ------- |
 |  Absence | 74185 |    92.8 |
 | Presence |  5721 |     7.2 |
 |   #Total | 79906 |   100.0 |
 |     <NA> |     0 |     0.0 |

### Data source                                                   
 |                    Source | Absence | Presence |
 | ------------------------- | ------- | -------- |
 |                   CrabBio |         |        1 |
 |                       FOS |         |       60 |
 |                     GFBio |    6012 |      398 |
 |                   GFVideo |         |        9 |
 |                 MSEA_Dive |    4884 |          |
 |                  MSEA_ROV |   11453 |     1488 |
 | NOAA_CoralSponge_Database |         |      442 |
 |                  PacTrawl |         |       20 |
 |                      RBCM |         |       42 |
 |             Shellfish_ROV |   51836 |     3259 |
 |             TannerCrabBio |         |        2 |
 |                    #Total |   74185 |     5721 |

### Gear type                                      
 |         Gear | Absence | Presence |
 | ------------ | ------- | -------- |
 |              |         |      117 |
 | Bottom trawl |    6012 |      335 |
 |         Dive |    4884 |          |
 |     Longline |         |       50 |
 |          ROV |   63289 |     5167 |
 |         Trap |         |       14 |
 |        Trawl |         |       38 |
 |       #Total |   74185 |     5721 |

### Year                                    
 | Year range | Absence | Presence |
 | ---------- | ------- | -------- |
 |  1995-1999 |         |        2 |
 |  2000-2004 |     635 |       98 |
 |  2005-2009 |   15469 |     1481 |
 |  2010-2014 |   21759 |     1235 |
 |  2015-2019 |   36322 |     2905 |
 |     #Total |   74185 |     5721 |

### Month                                
 |  Month | Absence | Presence |
 | ------ | ------- | -------- |
 |      1 |         |        7 |
 |      2 |         |        7 |
 |      3 |   10803 |      743 |
 |      4 |     281 |        9 |
 |      5 |    8357 |       92 |
 |      6 |   16951 |      492 |
 |      7 |   12352 |     1187 |
 |      8 |    1661 |      504 |
 |      9 |   16081 |     1885 |
 |     10 |    7699 |      791 |
 |     11 |         |        4 |
 | #Total |   74185 |     5721 |


