SELECT
H.Source AS SURVEY,
H.Key AS HKey,
H.Transect AS TRANSECT_ID,
H.Set_num AS SET_ID,
H.Start_year AS YEAR,
H.Start_mon AS MONTH,
H.Start_day AS DAY,
H.Stat_area AS MAJOR_STAT_AREA_CODE,
H.Start_lat_deg + H.Start_lat_min / 60 AS START_LATITUDE,
-(H.Start_long_deg + H.Start_long_min / 60) AS START_LONGITUDE,
H.Finish_lat_deg + H.Finish_lat_min / 60 AS END_LATITUDE,
-(H.Finish_long_deg + H.Finish_long_min / 60) AS END_LONGITUDE,
(IIF(IsNull(H.Min_depth),H.Max_depth,H.Min_depth) + IIF(IsNull(H.Max_depth),H.Min_depth,H.Max_depth))/2  AS DEPTH,
G.General_desc AS GEAR,
C.Species AS SPECIES_CODE
FROM
(Headers H INNER JOIN Catch C ON H.Key = C.HKey)
INNER JOIN GearCodes G ON H.gear_code = G.Gear_code
WHERE H.Start_lat_deg IS NOT NULL AND H.Finish_long_min IS NOT NULL;
