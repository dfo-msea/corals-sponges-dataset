dbs=$(ls *.mdb)
for db in $dbs; do
    out="${db%.mdb}.csv"
    mdb-export $db sponge > $out
done
