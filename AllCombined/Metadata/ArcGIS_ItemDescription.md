# Presence Absence Observations of Coral and Sponge Groups

## Summary
A geodatabase of coral and sponge presence absence observations on the BC shelf was created to develop and improve species distribution models and identify Sensitive Benthic Areas. Corals and sponges are sampled both directly (e.g. ROV observations) and indirectly (e.g. trawl by-catch) by a variety of DFO programs and partners. As there is no single source of information on corals and sponges, there was a need to compile observations from a variety of sources.

## Description
This coral and sponge geodatabase includes eleven point feature layers with presence and absence observations for each coral and sponge grouping: sponges, demosponges, glass sponges, calcareous sponges, corals, soft corals, stony corals, hydrocorals, sea pens, black and gorgonian corals. The observations contained in this dataset come from a variety of sources including DFO surveys (Groundfish data unit, Shellfish data unit, MSEA section) and external sources (commercial fisheries logs, RBC museum records, NOAA). Those data sources contained observations of corals and sponges captured from a variety of sampling methods and gear types: dive, ROV, traps, longline and bottom trawls.

The dataset was created using R and SQL code stored within this GitLab project: https://gitlab.com/dfo-msea/corals-sponges-dataset. For a summary of attributes from each feature layer see this report: https://gitlab.com/dfo-msea/corals-sponges-dataset/-/blob/master/AllCombined/Summary_CoralSponge_Dataset.md.

## Credits
Creator: Jessica Nephin (jessica.nephin@dfo-mpo.gc.ca). Major contributions to this dataset were made by Jackson Chu, DFO Pacific groundish and shellfish data units, Marine Spatial Ecology and Analysis section (MSEA) and external data sources: Fishery industry partners (joint surveys and commercial log records), NOAA (online coral and sponge database) and the Royal BC Museum.

## Use limitations
Confidentiality: Protected A. This dataset contains sponge and coral by-catch observations from commercial catch records.

## Extent
West  -136.449601    East  -121.644192
North  54.823856    South  37.337378
