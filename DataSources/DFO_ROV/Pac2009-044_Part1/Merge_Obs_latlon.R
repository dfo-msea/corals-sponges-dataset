require(rgdal)

#wd
#setwd("D:/SpeciesData/Inverts_Algae/Corals_Sponges/ROV/Pac2009-044")

# Projection
geoCRS <-  CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")
utmProj <-  "+proj=utm +zone=9 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"

#-------------------------------------------------------------------------------------#
# read obs
dat <- read.csv("Pac2009-044_obs.csv", header=T)

#clean - remove records where there is no substrate or species obs
dat$SpeciesID[is.na(dat$SpeciesID)] <- ""
dat$DominantSubstrate[is.na(dat$DominantSubstrate)] <- ""
df <- dat[dat$DominantSubstrate != "" | dat$SpeciesID != "",]
summary(df)

# extract live sponge reef observations from substrate columns
sponges <- df
sponges$SpeciesID <- ""
sponges$SpeciesID[sponges$DominantSubstrate == 12] <- "2I0"
sponges$SpeciesID[sponges$SubdominantSubstrate == 12] <- "2I0"
sponges <- sponges[sponges$SpeciesID == "2I0",]
df <- rbind(df, sponges)

# merge species data with GFBio species table to grab missing sci_names
spgf <- read.csv("SpeciesCodes_fromGFBio.csv", header=T, stringsAsFactors = F)
spgfdat <- merge(df, spgf, by.x="SpeciesID", by.y = "SPECIES_CODE", all.x=T)

# merge with coral and sponge groups
grps <- read.csv("AllCoralSponge_HartCodes_Groups.csv", header=T, stringsAsFactors = F)
dfgrp <- merge(spgfdat, grps[c("SPECIES_CODE","Group")], 
               by.x="SpeciesID", by.y = "SPECIES_CODE", all.x=T)
dfgrp$Group[is.na(dfgrp$Group)] <- ""

# species summary table
#sp <- dfgrp[!duplicated(dfgrp$SpeciesID),]
# export summary table
#write.csv(sp, "Pac2009-044_CoralSponge_SummaryTable.csv", row.names = F)

# add datetime column -- check for each survey
dfgrp$dtstr <- paste( dfgrp$TransectDate, dfgrp$TextTimeCode )
dfgrp$Datetime <- as.POSIXct( dfgrp$dtstr, format="%Y-%m-%d %H:%M:%S", tz="GMT" ) 
dfgrp$Date <- as.Date(dfgrp$Datetime)



#-------------------------------------------------------------------------------------#
# load nav data
nav <- read.csv("PAC2009-044 Part1 nav data.txt", header=F)

# add datetime column
nav$timetext <- substr(nav$V3, 1, 8)
nav$Date <- substr(nav$V2, 1, 10)
nav$dtstr <- paste( nav$Date, nav$timetext )
nav$Datetime <- as.POSIXct( nav$dtstr, format="%m/%d/%Y %H:%M:%S", tz="GMT" ) 
nav$utm_x <- nav$V4
nav$utm_y <- nav$V5
nav$Depth <- nav$V12


# locate where datetime skips more than 30 seconds
# these are likely locations of new dives/transects
nav <- nav[order(nav$Datetime),]
rownames(nav) <- 1:nrow(nav)
nav$Datetime2 <- nav$Datetime[c(2:nrow(nav), nrow(nav))]
nav$dif <- as.numeric(difftime(nav$Datetime2,nav$Datetime, units = "secs"))
nav$segment <- 0
for (i in 2:nrow(nav)){
  if(nav$dif[i-1] < 60) nav$segment[i] <- nav$segment[i-1]
  if(nav$dif[i-1] >= 60) nav$segment[i] <- nav$segment[i-1] + 1
}
# number of segments
length(unique(nav$segment))


# expand lat,lon,depth within segments 
exfunc <- function( s ){
  #require
  require(zoo)
  # get segment df
  navdf <- nav[nav$segment == s,]
  if( nrow(navdf) > 1 ){
    # fill times
    filltimes <- data.frame(Datetime=seq(min(navdf$Datetime), max(navdf$Datetime), by="secs"))
    # join with nav
    allsecs <- merge(filltimes, navdf, by="Datetime", all.x=T) 
    # Approximate lat long positions and depth
    allsecs$utm_y <- na.approx(allsecs$utm_y)
    allsecs$utm_x <- na.approx(allsecs$utm_x)
    allsecs$Depth <- na.approx(allsecs$Depth)
    return(allsecs)
  }
}

# create cluster object
num_cores <- parallel::detectCores() - 3
cl <- parallel::makeCluster(num_cores)
## make variables available to cluster within function environment
parallel::clusterExport(cl, varlist=c("nav"), envir = environment())
# apply expanding by segment function
exlist <- parallel::parLapply(cl, unique(nav$segment), exfunc )
## stop cluster
parallel::stopCluster(cl)

# recombine in dataframe
allnavdat <- do.call("rbind",exlist)
# save
save(allnavdat, file = "Nav.RData")


#-------------------------------------------------------------------------------------#
# merge with nav
spdf <- merge(dfgrp, allnavdat[c("Datetime","utm_x","utm_y","Depth")], 
              by="Datetime", all.x=T)

# investiage records without lat/long
summary(spdf)
test <- spdf[is.na(spdf$utm_y),]
table(test$Date)

# final dataframe without missing lat/lon
dat <- spdf[!is.na(spdf$utm_y),]

# percent missing
nrow(test)/nrow(spdf)
# 0.3% missing


#-------------------------------------------------------------------------------------#
# Convert utm to lat and lon
alldat <- dat
coordinates(alldat) <- ~utm_x+utm_y
proj4string(alldat) <- utmProj
alldat <- spTransform(alldat, "+proj=longlat +datum=WGS84")
dat <- data.frame(Longitude = coordinates(alldat)[,1], 
                  Latitude = coordinates(alldat)[,2],
                  alldat@data)



#-------------------------------------------------------------------------------------#
# Set attributes name for consistency between surveys

# current names
names(dat)

# add missing columns
dat$Survey <- "Pac2009-044"


# subset dataset
dat <- dat[c("Survey", "Datetime", "Longitude", "Latitude", "Depth",
             "DominantSubstrate", "DominantPercent", 
             "SubdominantSubstrate","SubdominantPercent",
             "SpeciesID", "SPECIES_DESC", "SPECIES_SCIENCE_NAME", "Group")]

# new names
names(dat) <- c("Survey", "Datetime", "Longitude", "Latitude", "Depth",
                "DominantSubstrate", "DominantPercent", 
                "SubdominantSubstrate","SubdominantPercent",
                "Species_Code", "CommonName", "ScientificName", "Group")

# remove empty species id == 848
dat <- dat[dat$DominantSubstrate != "" | dat$Species_Code != "848",]

# check
dim(dat)
head(dat)
summary(dat)


#-------------------------------------------------------------------------------------#
# all data 

#export
write.csv(dat, "Pac2009-044_Part1_SpeciesSubstrateObs_xy.csv")
# shapefile
alldat <- dat
coordinates(alldat) <- ~Longitude+Latitude
proj4string(alldat) <- "+proj=longlat +datum=WGS84"
alldat <- spTransform(alldat, geoCRS)
writeOGR(alldat, dsn="Shapefiles", layer="Pac2009-044_part1_SpeciesSubstrateObs",
         driver="ESRI Shapefile", overwrite_layer =T)


#-------------------------------------------------------------------------------------#
# just species data

# just records with species obs
spdat <- dat[!is.na(dat$Species_Code),]

# shapefile
coordinates(spdat) <- ~Longitude+Latitude
proj4string(spdat) <- "+proj=longlat +datum=WGS84"
spdat <- spTransform(spdat, geoCRS)
writeOGR(spdat, dsn="Shapefiles", layer="Pac2009-044_part1_SpeciesObs",
         driver="ESRI Shapefile", overwrite_layer =T)

#-------------------------------------------------------------------------------------#
# just species data

# just coral and sponge gourps
cs <- dat[dat$Group != "",]

# shapefile
coordinates(cs) <- ~Longitude+Latitude
proj4string(cs) <- "+proj=longlat +datum=WGS84"
cs <- spTransform(cs, geoCRS)
writeOGR(cs, dsn="Shapefiles", 
         layer="Pac2009-044_part1_SpeciesObs_CoralSpongePresence",
         driver="ESRI Shapefile", overwrite_layer =T)

